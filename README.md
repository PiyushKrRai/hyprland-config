# Piyush's Hyprland Config

Hyprland config made by me to clone my desktop

## Installation

Install the following packages using yay

```bash
  pacman -S hyprland hypridle hyprlock swww fish kitty neofetch nvim waybar wofi bluetuith

```

## Screenshots

![App Screenshot](https://gitlab.com/PiyushKrRai/hyprland-config/-/raw/main/example.jpeg?ref_type=heads)
