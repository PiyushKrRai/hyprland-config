if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -g fish_greeting 'Hello Piyush'

alias cls='clear; fastfetch'
alias unlock='sudo rm /var/lib/pacman/db.lck'
alias cleanup='sudo pacman -R (pacman -Qtdq)'
alias nano='vim'
alias btstatus='systemctl status bluetooth'
alias btstart='sudo systemctl start bluetooth'
alias update='sudo pacman -Syyu'
alias search='pacman -Ss'
alias install='sudo pacman -S'
alias remove='sudo pacman -R'
alias purge='sudo pacman -Rcns'
alias fixgpgme='sudo rm -R /var/lib/pacman/sync; sudo -E pacman -Syu'



cls

# bun
set --export BUN_INSTALL "$HOME/.bun"
set --export PATH $BUN_INSTALL/bin $PATH
