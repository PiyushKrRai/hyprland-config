#!/usr/bin/env bash
bash ~/.config/i3-agenda/i3agenda-onscroll.sh down 
i3-agenda -c ~/.google_credentials.json --skip $(cat ~/.config/i3-agenda/i3-agenda-skip.tmp || echo 0)